package com.raphanum.gifsearcher.api;

import com.raphanum.gifsearcher.model.GiphyData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GiphyApiInterface {

    @GET("/v1/gifs/search")
    Call<GiphyData> getSearchResult(@Query("api_key") String apiKey,
                                    @Query("q") String term,
                                    @Query("limit") int limit,
                                    @Query("rating") String rating);

    @GET("v1/gifs/trending")
    Call<GiphyData> getTrending(@Query("api_key") String apiKey,
                                @Query("limit") int limit,
                                @Query("rating") String rating);
}

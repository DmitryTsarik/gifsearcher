package com.raphanum.gifsearcher;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.raphanum.gifsearcher.model.GifObject;

import java.util.List;

public class RetainedFragment extends Fragment {

    private List<GifObject> mGifObjectList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void setGifObjectList(List<GifObject> gifObjectList)
    {
        mGifObjectList = gifObjectList;
    }

    public List<GifObject> getGifObjectList() {
        return mGifObjectList;
    }

}

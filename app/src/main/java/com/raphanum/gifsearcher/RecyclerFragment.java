package com.raphanum.gifsearcher;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.raphanum.gifsearcher.model.GifObject;

import java.util.List;

//TODO: Отслеживать состояния запросов.
public class RecyclerFragment extends Fragment {

    private GifObjectListAdapter mAdapter;
    private List<GifObject> mGifObjectList;
    private SharedPreferences mSharedPreferences;
    private ProgressBar mProgressBar;
    private RetainedFragment mRetainedFragment;
    private SwipeRefreshLayout mRefreshLayout;
    private RecyclerView mRecyclerView;
    private View.OnTouchListener mOnTouchListener;

    public RecyclerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycler, container, false);
        mRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh_container);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.gif_recyclerview);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(rootView.getContext());
        mProgressBar = (ProgressBar)rootView.findViewById(R.id.loading_progressbar);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initRecyclerView();
        initRefreshLayout();
        initOnResponseListener();
    }

    @Override
    public void onStart() {
        super.onStart();
        initRetainedFragment();
    }

    @Override
    public void onStop() {
        super.onStop();
        mRetainedFragment.setGifObjectList(mGifObjectList);
    }

    private void initOnResponseListener() {
        RetrofitHelper.getInstance().setOnResponseListener(new RetrofitHelper.OnResponseListener() {
            @Override
            public void onResponse(List<GifObject> imageList) {
                mGifObjectList = imageList;
                mAdapter.setGifObjectList(mGifObjectList);
                mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure() {
                mProgressBar.setVisibility(View.INVISIBLE);
                showToast("Failure");
            }
        });
    }

    //TODO: Исправить дёрганность интерфейса.
    private void initRecyclerView() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        } else {
            mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
        }
        mAdapter = new GifObjectListAdapter(mGifObjectList);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        Intent intent = new Intent(getContext(), GifActivity.class);
                        String url = mGifObjectList.get(position).getImages().getOriginalImage().getUrl();
                        intent.putExtra("url", url);
                        startActivity(intent);
                    }
                }));

        mRecyclerView.setOnTouchListener(mOnTouchListener);

        mAdapter.setOnLoadImageListener(new GifObjectListAdapter.OnLoadImageListener() {
            @Override
            public void loadImage(ImageView imageView, String url) {
                GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imageView);
                Glide.with(getContext().getApplicationContext())
                        .load(url)
                        .placeholder(R.drawable.ic_gif_black_24dp)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageViewTarget);
            }
        });
    }

    private void initRetainedFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        mRetainedFragment = (RetainedFragment) fragmentManager.findFragmentByTag("retainedFragment");

        if (mRetainedFragment != null && mRetainedFragment.getGifObjectList() != null) {
            mGifObjectList = mRetainedFragment.getGifObjectList();
            mAdapter.setGifObjectList(mGifObjectList);
        } else {
            mRetainedFragment = new RetainedFragment();
            fragmentManager.beginTransaction().add(mRetainedFragment, "retainedFragment").commit();
            mProgressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().load();
        }
    }

    private void initRefreshLayout() {
        mRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mRefreshLayout.setRefreshing(true);
                RetrofitHelper.getInstance().load();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mRefreshLayout.setRefreshing(false);
                    }
                }, 1000);
            }
        });
    }

    public String getRating() {
        if (mSharedPreferences.getBoolean(getString(R.string.key_censorship), false)) {
            return Constants.RATING_PG;
        }
        return Constants.RATING_R;
    }

    public int getResultLimit() {
        return Integer.parseInt(mSharedPreferences.getString(getString(R.string.key_result_limit), "50"));
    }

    public void setOnTouchRecyclerViewListener(View.OnTouchListener listener) {
        mOnTouchListener = listener;
    }

    private void showToast(String string) {
        Toast.makeText(getContext(), string, Toast.LENGTH_SHORT).show();
    }
}

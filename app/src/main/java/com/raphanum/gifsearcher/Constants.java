package com.raphanum.gifsearcher;

public class Constants {
    public static final String API_KEY_BETA = "dc6zaTOxFJmzC";
    public static final String BASE_URL = "https://api.giphy.com/";
    public static final String SEARCH_QUERY = "query";
    public static final String RATING_PG = "pg";
    public static final String RATING_R = "r";
}

package com.raphanum.gifsearcher.model;

import com.google.gson.annotations.SerializedName;

public class GifImage {

    @SerializedName("fixed_height_still")
    private GifImageParameters previewImage;
    @SerializedName("original")
    private GifImageParameters originalImage;

    public GifImageParameters getPreviewImage() {
        return previewImage;
    }

    public GifImageParameters getOriginalImage() {
        return originalImage;
    }
}

package com.raphanum.gifsearcher.model;

import com.google.gson.annotations.SerializedName;

public class GifObject {

    @SerializedName("id")
    private String imageId;
    @SerializedName("slug")
     private String imageName;
    @SerializedName("images")
    private GifImage images;

    public String getImageId() {
        return imageId;
    }

    public String getImageName() {
        return imageName;
    }

    public GifImage getImages() {
        return images;
    }
}

package com.raphanum.gifsearcher.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GiphyData {

    @SerializedName("data")
    List<GifObject> data;

    public List<GifObject> getData() {
        return data;
    }
}

package com.raphanum.gifsearcher;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.raphanum.gifsearcher.model.GifObject;

import java.util.List;

public class GifObjectListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<GifObject> mGifObjectList;
    private OnLoadImageListener mOnLoadImageListener;

    public GifObjectListAdapter(List<GifObject> gifObjectList) {
        mGifObjectList = gifObjectList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.gif_item, parent, false);
        vh = new PreviewGifViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (mGifObjectList != null) {
            ImageView imageView = ((PreviewGifViewHolder)holder).mImageView;
            mOnLoadImageListener.loadImage(imageView, mGifObjectList.get(position).getImages().getPreviewImage().getUrl());
        }
    }

    @Override
    public int getItemCount() {
        if (mGifObjectList != null) {
            return mGifObjectList.size();
        }
        return 0;
    }

    public static class PreviewGifViewHolder extends RecyclerView.ViewHolder {

        ImageView mImageView;

        public PreviewGifViewHolder(View itemView) {
            super(itemView);

            mImageView = (ImageView) itemView.findViewById(R.id.gif_item_imageview);
        }
    }

    public void setGifObjectList(List<GifObject> gifObjectList) {
        this.mGifObjectList = gifObjectList;
        notifyDataSetChanged();
    }

    public void setOnLoadImageListener(OnLoadImageListener onLoadImageListener) {
        mOnLoadImageListener = onLoadImageListener;
    }

    public interface OnLoadImageListener {
        void loadImage(ImageView imageView, String url);
    }
}

package com.raphanum.gifsearcher;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.raphanum.gifsearcher.api.GiphyApiInterface;
import com.raphanum.gifsearcher.model.GiphyData;

import retrofit2.Call;

public class MainActivity extends AppCompatActivity {

    private EditText mEditTextSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initEditTextSearch();
        initRecyclerFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ImageView menuImageView = (ImageView)findViewById(R.id.menu_imageview);
        PopupMenu popupMenu = new PopupMenu(getApplicationContext(), menuImageView);
        popupMenu.getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void initRecyclerFragment() {
        final RecyclerFragment fragment = (RecyclerFragment)getSupportFragmentManager().findFragmentById(R.id.main_recycler_fragment);
        RetrofitHelper.getInstance().setOnLoadImagesListener(new RetrofitHelper.OnLoadImagesListener() {
            @Override
            public Call<GiphyData> createRequest(GiphyApiInterface giphyApiInterface) {
                return giphyApiInterface.getTrending(
                        Constants.API_KEY_BETA, fragment.getResultLimit(), fragment.getRating());
            }

            @Override
            public void onPostResponse(int listSize) {

            }
        });

        fragment.setOnTouchRecyclerViewListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard();
                mEditTextSearch.clearFocus();
                return false;
            }
        });

        RetrofitHelper.getInstance().load();
    }

    private void initEditTextSearch() {
        mEditTextSearch = (EditText) findViewById(R.id.search_edittext);

        mEditTextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    hideKeyboard();
                    startSearchResultActivity();
                }
                return false;
            }
        });
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) mEditTextSearch.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEditTextSearch.getWindowToken(), 0);
    }

    public void onSearchClick(View view) {
        startSearchResultActivity();
    }

    public void showMenu(View view) {
        PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
        popupMenu.inflate(R.menu.menu_main);
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_settings:
                        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                        startActivity(intent);
                        return true;
                }
                return false;
            }
        });
    }

    private void startSearchResultActivity() {
        String query = mEditTextSearch.getText().toString();
        if (!query.replace(" ", "+").equals("")) {
            Intent intent = new Intent(MainActivity.this, SearchResultActivity.class);
            intent.putExtra(Constants.SEARCH_QUERY, query);
            startActivity(intent);
        }
    }
}

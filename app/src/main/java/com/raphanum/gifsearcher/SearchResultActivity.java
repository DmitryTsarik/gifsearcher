package com.raphanum.gifsearcher;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.raphanum.gifsearcher.api.GiphyApiInterface;
import com.raphanum.gifsearcher.model.GiphyData;

import retrofit2.Call;

public class SearchResultActivity extends AppCompatActivity {

    private String mQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        mQuery = (String)getIntent().getExtras().get(Constants.SEARCH_QUERY);
        setTitle(mQuery);
        initRecyclerFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        onNavigateUp();
    }

    private void initRecyclerFragment() {
        final RecyclerFragment fragment = (RecyclerFragment)getSupportFragmentManager().findFragmentById(R.id.search_result_recycler_fragment);
        RetrofitHelper.getInstance().setOnLoadImagesListener(new RetrofitHelper.OnLoadImagesListener() {
            @Override
            public Call<GiphyData> createRequest(GiphyApiInterface giphyApiInterface) {
                return giphyApiInterface.getSearchResult(
                        Constants.API_KEY_BETA, mQuery, fragment.getResultLimit(), fragment.getRating());
            }

            @Override
            public void onPostResponse(int listSize) {
                if (listSize == 0) {
                    findViewById(R.id.not_found_textview).setVisibility(View.VISIBLE);
                }
            }
        });
        RetrofitHelper.getInstance().load();
    }
}

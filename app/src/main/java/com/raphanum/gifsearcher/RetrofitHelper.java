package com.raphanum.gifsearcher;

import com.raphanum.gifsearcher.api.GiphyApiInterface;
import com.raphanum.gifsearcher.model.GifObject;
import com.raphanum.gifsearcher.model.GiphyData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitHelper {
    private static RetrofitHelper mInstance;
    private GiphyApiInterface mGiphyApi;
    private OnResponseListener mOnResponseListener;
    private OnLoadImagesListener mOnLoadImagesListener;

    private RetrofitHelper() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mGiphyApi = retrofit.create(GiphyApiInterface.class);
    }

    public static RetrofitHelper getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitHelper();
        }
        return mInstance;
    }

    public void load() {
        Call<GiphyData> call = mOnLoadImagesListener.createRequest(mGiphyApi);
        call.enqueue(new Callback<GiphyData>() {
            @Override
            public void onResponse(Call<GiphyData> call, Response<GiphyData> response) {
                mOnResponseListener.onResponse(response.body().getData());
                mOnLoadImagesListener.onPostResponse(response.body().getData().size());
            }

            @Override
            public void onFailure(Call<GiphyData> call, Throwable t) {
                mOnResponseListener.onFailure();
            }
        });
    }

    public interface OnResponseListener {
        void onResponse(List<GifObject> imageList);

        void onFailure();
    }

    public interface OnLoadImagesListener {
        Call<GiphyData> createRequest(GiphyApiInterface giphyApiInterface);

        void onPostResponse(int listSize);
    }

    public void setOnResponseListener(OnResponseListener listener) {
        mOnResponseListener = listener;
    }

    public void setOnLoadImagesListener(OnLoadImagesListener listener) {
        mOnLoadImagesListener = listener;
    }
}
